#include <fstream>
#include <string>
#include <iostream>

#include "santa.h"



int main(int argc, char *argv[])
{
	std::string filename;
	std::ifstream data_stream;
	std::cout << "Argc: " << argc << std::endl;
	for (int i = 0; i < argc; i++)
		std::cout << argv[i] << std::endl;

	if (argc < 2)
	{
		std::cout << "Usage: /n/t/tsanta input.dat" << std::endl;
		return 1;
	}

	filename = argv[1];
	data_stream.open(filename, std::ifstream::in);
	if (!data_stream.is_open())
	{
		std::cout << "Cannot open data file " << filename << std::endl;
		return 2;
	};
	Santa FunnySanta(0);
	FunnySanta.setBasementLevel(-1);
	char step;

	while (data_stream.get(step)) {
		FunnySanta.updatePosition(step);
		FunnySanta.checkPosition();

	}
	std::cout << "Floor: " << FunnySanta.getCurrentPosition() << std::endl;
	std::cout << "Enter basement step: " << FunnySanta.getBasementEnterStep() << std::endl;
	return 0;
}