#include "santa.h"
#include <iostream>

Santa::Santa(int startPositionLevel) :
	mCurrentPosition(startPositionLevel),
	mSteps(0),
	mBasementLevel(0),
	isInBasement(false)
{}

Santa::~Santa()
{}

void Santa::updatePosition(char motion)
{
	switch (motion) {
	case '(':
		goUp();
		break;
	case ')':
		goDown();
		break;
	default:
		std::cout << "Wrong input" << std::endl;
		break;
	};
}

void Santa::checkPosition() {
	if (isInBasement)
		return;
	
	if( mCurrentPosition == mBasementLevel )
	{
		isInBasement = true;
		mEnterBasementStep = mSteps;
	}
}

int Santa::getCurrentPosition() {
	return mCurrentPosition;
}

int Santa::getBasementEnterStep() {
	return mEnterBasementStep;
};

void Santa::setBasementLevel(int level) {
	mBasementLevel = level;
}

void Santa::goUp() {
	mSteps++;
	mCurrentPosition++;
}

void Santa::goDown() {
	mSteps++;
	mCurrentPosition--;
}