#pragma once

class Santa{
	public:
		Santa(int startPositionLevel);
		~Santa();
		void setBasementLevel(int level);

		void updatePosition(char motion);
		void checkPosition();

		int getCurrentPosition();
		int getBasementEnterStep();

		
	private:
		void goUp();
		void goDown();
		
		int mCurrentPosition;
		int mSteps;
		int mBasementLevel;
		bool isInBasement;
		int mEnterBasementStep;
};
