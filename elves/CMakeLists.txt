cmake_minimum_required(VERSION 3.8)

set(APPLICATION elves)
project(${APPLICATION})

set(SRC_DIR ${PROJECT_SOURCE_DIR}//src)
set(INC_SRC_DIR ${PROJECT_SOURCE_DIR}//src)
set(SRC ${SRC} 
	${SRC_DIR}//main.cpp
	${SRC_DIR}//elves.h
	${SRC_DIR}//elves.cpp)
include_directories(${INC_SRC_DIR})
add_executable(${APPLICATION} ${SRC})
